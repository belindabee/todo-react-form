import React, { Component } from "react"
import { Link } from "react-router-dom"
class LoginPage extends Component {
  state = {
    email: "",
    password: ""
  }
  render() {
    return (
      <div>
        <h1>Login</h1>
        <form>
          <input
            type="email"
            name="email"
            placeholder="please enter your email here"
            value={this.state.email}
            onChange={this.handleChange}
          />
          <input
            type="password"
            name="password"
            placeholder="please enter your password here"
            value={this.state.password}
            onChange={this.handleChange}
          />
        </form>
        <Link to="/">
          <button>Back</button>
        </Link>
      </div>
    )
  }
}

export default LoginPage
