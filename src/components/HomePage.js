import React, { Component } from "react"
import { Link } from "react-roter-dom"

class HomePage extends Component {
  render() {
    return (
      <div>
        <h1>Home Page</h1>
        <button>
          <Link to="/register">register</Link>
        </button>
        <Link to="/login">login</Link>
      </div>
    )
  }
}

export default HomePage
