import React, { Component } from "react"
import { Link } from "react-router-dom"

class RegisterPage extends Component {
  constructor() {
    super()
    this.state = {
      email: "",
      password: "",
      fullname: "",
      hasAggred: false
    }
  }

  onSubmit = e => {
    e.preventDefault()
    const formdata = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password
    }
  }

  onChange = e => this.setState({ [e.target.name]: e.target.value })

  render() {
    return (
      <div>
        <h1>Please Sign In when you already have Account</h1>
        <form onSubmit={this.onSubmit} style={{ display: "felx" }}>
          <label className="FormFiled_label" htmlFor="fullname">
            {" "}
            Full Name{" "}
          </label>
          <input
            type="text"
            id="fullname"
            className="FormField_input"
            placeholder="Enter your full name"
            value={this.state.name}
            onChange={this.onChange}
          />
          <div className="FormCenter">
            <label className="FormField_label" htmlFor="email">
              Email
            </label>
            <input
              type="email"
              id="email"
              className="FormField_input"
              placeholder="Enter your email address"
              name="email"
              value={this.state.email}
              onChange={this.onChange}
            />
          </div>
          <div className="FormField">
            <label className="FormField_label" htmlFor="password">
              Password
            </label>
            <input
              type="password"
              id="password"
              className="FormField_input"
              placeholder="Enter your Password"
              name="password"
              value={this.state.password}
              onChange={this.onChange}
            />
            <input type="submit" value="submit" style={{ flex: 1 }} />
          </div>
        </form>
        <Link to="/">back</Link>
      </div>
    )
  }
}
export default RegisterPage
