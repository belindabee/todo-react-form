import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from "react-router-dom"
import { Provider } from "react-redux"
import store from "./store"

import HomePage from "./components/HomePage"
import RegisterPage from "./components/RegisterPage"
import DashboardPage from "./components/DashboardPage"
import "./App.css"

 class App extends Component {
  render() {
    return (
      <Provider store={store}>
      <Router>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/registerPage" component={RegisterPage} />
        <Route exact path="/dashboard" component={DashboardPage} />
      </Router>
    </Provider>
    )
  }
}
export default App

