import axios from "axios"
import {REGISTER_SUCCESS, REGISTER_FAIL} from "../types"

import { createStore, applyMiddleware, compose } from "redux"
import thunk from "redux-thunk"
import { composeWithDevTools } from "redux-devtools-extension"
import rootReducer from "./reducers"

const middleware = [thunk]

const store = createStrore(
  rootReducer,
  composeWithDevTools(...middleware))
  )

  export default store